import './App.css'
import logo from './logo.webp'

export default function App() {
  return (
    <main>
      <a href="https://lancolatech.co.ke">
        <img
          src={logo}
          height={50}
        />
      </a>

    </main>
  )
}
